import React from 'react'
import { Container, Button, Table,Badge,Row, Col } from 'react-bootstrap'



function TableData(props) {

    return (
        <Container>
             <Button
                    //  disabled={props.items.amount === 0}
                    //  onClick={()=>{
                    //      props.onReset()
                    //  }}
                    variant="info"
                    className="mt-2 mb-2">Reset
             </Button>
             <Badge variant="warning" className="mx-2" >Count {props.items.length}  </Badge>

            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Food</th>
                    <th>Amount</th>
                    <th>Price</th>
                    <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                
                {
                   props.items.map((item,idx)=>
                     <tr key={idx} hidden={item.amount === 0}>
                     <td>{item.id}</td>
                     <td>{item.title}</td>
                     <td>{item.amount}</td>
                     <td>{item.price}</td>
                     <td>{item.total}</td>
                     </tr>
                )
                }
                    
                </tbody>
               
            </Table>
        </Container>
    )
}

export default TableData
